

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.Component;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Window;
import java.awt.Color;

public class View {

	public JFrame frame;
	
	private Bank bank;

	

	/**
	 * Create the application.
	 */
	public View(Bank bank) {
		this.bank=bank;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.menu);
		frame.setBounds(100, 100, 700, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnClient = new JButton("CLIENT");
		btnClient.setFont(new Font("Arial", Font.BOLD, 25));
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try 
				{
					ClientView cv = new ClientView (bank);
					cv.setVisible(true);
				} catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, e);
				}
				
			}
		});
		btnClient.setBounds(105, 109, 167, 126);
		frame.getContentPane().add(btnClient);
		
		JButton btnAccount = new JButton("ACCOUNT");
		btnAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg1) {
					
				try 
				{
					AccountView av = new AccountView(bank);
					av.setVisible(true);
				} catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, e);
				}
				
			}
		});
		btnAccount.setFont(new Font("Arial", Font.BOLD, 25));
		btnAccount.setBounds(413, 109, 167, 126);
		frame.getContentPane().add(btnAccount);
	}
}
