
public class SavingAccount extends Account{
	
	
	
	private int noDeposit;
	private double interestRate;
	
	
	
	public int getNoDeposit() {
		return noDeposit;
	}

	public void setNoDeposit(int noDeposit) {
		this.noDeposit = noDeposit;
	}
	
	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public SavingAccount(String iban,String holder,int sum,int noDeposit) {
		super(iban, holder,sum);
		this.noDeposit=noDeposit;
		// TODO Auto-generated constructor stub
	}
	

}
