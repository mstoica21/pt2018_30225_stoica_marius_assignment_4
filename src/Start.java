import java.awt.EventQueue;
import java.util.HashMap;

public class Start {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		Bank bank = new Bank();
		
		/*Client c = null;
		bank.addClient(c);*/
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					View window = new View(bank);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

}
