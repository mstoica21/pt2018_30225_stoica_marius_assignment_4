import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JToolBar;
import javax.swing.JScrollPane;

public class ClientView extends JFrame {
	
	
	
	
	private JPanel contentPane;
	private JTextField tfCod;
	private JTextField tfNume;
	private JTextField tfTel;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	private Bank bank;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public ClientView(Bank bank) {
		this.bank=bank;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblCod = new JLabel("COD");
		lblCod.setFont(new Font("Arial", Font.PLAIN, 20));
		lblCod.setBounds(28, 44, 94, 46);
		contentPane.add(lblCod);
		
		JLabel lblNume = new JLabel("NUME");
		lblNume.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNume.setBounds(28, 103, 94, 46);
		contentPane.add(lblNume);
		
		JLabel lblTel = new JLabel("PHONE");
		lblTel.setFont(new Font("Arial", Font.PLAIN, 20));
		lblTel.setBounds(28, 162, 94, 46);
		contentPane.add(lblTel);
		
		tfCod = new JTextField();
		tfCod.setBounds(134, 58, 125, 22);
		contentPane.add(tfCod);
		tfCod.setColumns(10);
		
		tfNume = new JTextField();
		tfNume.setColumns(10);
		tfNume.setBounds(134, 117, 125, 22);
		contentPane.add(tfNume);
		
		tfTel = new JTextField();
		tfTel.setColumns(10);
		tfTel.setBounds(134, 176, 125, 22);
		contentPane.add(tfTel);
		
		
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg1) {
				
				
				
				
				String readCod,readNume,readTel;
				
				readCod=tfCod.getText();
				readNume=tfNume.getText();
				readTel=tfTel.getText();
				
				Client c = new Client (readCod,readNume,readTel);
	
				bank.addClient(c);
				
				
				//System.out.println(c.getCod() + " " + c.getNume() + " " + c.getPhone() );
				
				
				
				
				
				
			}
		});
		btnAdd.setFont(new Font("Arial", Font.BOLD, 15));
		btnAdd.setBounds(28, 245, 72, 38);
		contentPane.add(btnAdd);
		
		JButton btnUpdate = new JButton("UPD");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg2) {
				
				
				String readCod,readNume,readTel;
				
				readCod=tfCod.getText();
				readNume=tfNume.getText();
				readTel=tfTel.getText();
				
				Client c = new Client (readCod,readNume,readTel);
				
				bank.update(c);
			}
		});
		btnUpdate.setFont(new Font("Arial", Font.BOLD, 15));
		btnUpdate.setBounds(142, 245, 72, 38);
		contentPane.add(btnUpdate);
		
		JButton btnRem = new JButton("REM");
		btnRem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg3) {
				
				
				String readCod,readNume,readTel;
				
				readCod=tfCod.getText();
				readNume=tfNume.getText();
				readTel=tfTel.getText();
				
				Client c = new Client (readCod,readNume,readTel);
				
				bank.delete(c);
			}
		});
		btnRem.setFont(new Font("Arial", Font.BOLD, 15));
		btnRem.setBounds(256, 245, 72, 38);
		contentPane.add(btnRem);
		
		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try 
				{
					setVisible(false);
				} catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, e);
				}
				
			}
		});
		btnBack.setFont(new Font("Arial", Font.BOLD, 15));
		btnBack.setBounds(28, 343, 82, 33);
		contentPane.add(btnBack);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(357, 44, 287, 226);
		contentPane.add(scrollPane);
		
		JButton btnDisplay = new JButton("DISPLAY");
		btnDisplay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg4) {
				
				//bank.show();
				
				
				
				String [] antet = {"COD","NUME","PHONE"};
				DefaultTableModel dtm=new DefaultTableModel(antet,0);
				
				table = new JTable(dtm);
				scrollPane.setViewportView(table);
				
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				
				
				
				for (Client client:bank.HMap.keySet())
				{
					model.addRow(new Object [] {client.getCod(),client.getNume(),client.getPhone()});
				}
				
			}
		});
		btnDisplay.setFont(new Font("Arial", Font.BOLD, 20));
		btnDisplay.setBounds(447, 317, 140, 38);
		contentPane.add(btnDisplay);
		
		
		
	}
}
