
public interface BankProc {
	
	/**
	 * @pre client is not null
	 * @post 
	 * @param c
	 */
	
	
	public void addClient (Client c);
	public void delete (Client c);
	public void addAccount (Account a);
	public void updateAccount (Account a);
	public void removeAccount (String holder, String iban);
	public void update (Client c);
}
