import java.util.ArrayList;
import java.util.HashMap;

public class Bank implements BankProc {
		
	HashMap <Client,ArrayList <Account>> HMap = new HashMap <> ();
	
	public HashMap<Client, ArrayList<Account>> getHMap() {
		return HMap;
	}


	public void setHMap(HashMap<Client, ArrayList<Account>> hMap) {
		HMap = hMap;
	}
	
	public void addClient (Client c)
	{
		
		HMap.put(c,new ArrayList <Account> ());
		assert c!=null :"Client is null";
		
		
	}
	
	
	
	public void update (Client c)
	{
		
		for(Client client:HMap.keySet())
		{
			if (client.getCod().equals(c.getCod()))
			{
				client.setNume(c.getNume());
				client.setPhone(c.getPhone());
			}
		}
	}
	
	public void delete (Client c)
	{
		for(Client client:HMap.keySet())
		{
			if (client.getCod().equals(c.getCod()))
			{
				HMap.remove(client);
			}
		}
	}
	
	public void withdraw (int sum,String holder,String readiban)
	{
		for (Client client : HMap.keySet())
		{
			if (client.getCod().equals(holder)) 
			{
				for(Account acc:HMap.get(client))
				{
					if (acc.getIban().equals(readiban))
					{
						int a,dif;
						a=acc.getSum();
						dif=a-sum;
						acc.setSum(dif);
					}
				}
				
			}
		}
	}
	
	



	public void addAccount (Account a)
	{
		ArrayList <Account> arr = new ArrayList ();
		for (Client client:HMap.keySet())
		{
			if (client.getCod().equals(a.getHolder())) 
			{
				for(Account acc:HMap.get(client))
				{
					arr.add(acc);
				}
				arr.add(a);
				HMap.put(client,arr);
			}
		}
	}
	
	public void updateAccount (Account a)
	{
		for (Client client:HMap.keySet())
		{
			if (client.getCod().equals(a.getHolder())) 
			{
				for(Account acc:HMap.get(client))
				{
					if (acc.getIban().equals(a.getIban()))
					{
						acc.setIban(a.getHolder());
					}
				}
			}
		}
	}
	
	public void removeAccount (String holder, String iban)
	{
		for (Client client:HMap.keySet())
		{
			if (client.getCod().equals(holder)) 
			{
				for(Account acc:HMap.get(client))
				{
					if (acc.getIban().equals(iban))
					{
						HMap.get(client).remove(acc);
					}
				}
			}
		}
	}
	
	public void deposit (String iban,String holder,int sum)
	{
		for (Client client:HMap.keySet())
		{
			if (client.getCod().equals(holder)) 
			{
				for(Account acc:HMap.get(client))
				{
					if (acc.getIban().equals(iban))
					{
						acc.setSum(sum);
					}
				}
			}
		}
	}
	
	public void display ()
	{
		for (Client client:HMap.keySet())
		{
			
			for(Account acc:HMap.get(client))
			{
					System.out.println(acc.getHolder() + " " + acc.getIban() + " " + acc.getSum());
			}
				
			
		}
	}
}
