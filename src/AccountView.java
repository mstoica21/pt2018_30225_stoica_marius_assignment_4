import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class AccountView extends JFrame {
	
	
	
	
	private JPanel contentPane;
	private JTextField tfIban;
	private JTextField tfHolder;
	private JRadioButton btnSpending;
	private JTextField tfSum;
	
	private Bank bank;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public AccountView(Bank bank) {
		this.bank=bank;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try 
				{
					setVisible(false);
				} catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, e);
				}
			}
		});
		btnBack.setFont(new Font("Arial", Font.BOLD, 15));
		btnBack.setBounds(28, 352, 94, 38);
		contentPane.add(btnBack);
		
		JLabel lblIban = new JLabel("IBAN");
		lblIban.setFont(new Font("Arial", Font.PLAIN, 20));
		lblIban.setBounds(28, 76, 94, 46);
		contentPane.add(lblIban);
		
		JLabel lblType = new JLabel("TYPE");
		lblType.setFont(new Font("Arial", Font.PLAIN, 20));
		lblType.setBounds(28, 17, 94, 46);
		contentPane.add(lblType);
		
		JLabel lblHolder = new JLabel("HOLDER");
		lblHolder.setFont(new Font("Arial", Font.PLAIN, 20));
		lblHolder.setBounds(28, 123, 94, 46);
		contentPane.add(lblHolder);
		
		tfIban = new JTextField();
		tfIban.setColumns(10);
		tfIban.setBounds(134, 90, 125, 22);
		contentPane.add(tfIban);
		
		tfHolder = new JTextField();
		tfHolder.setColumns(10);
		tfHolder.setBounds(134, 137, 125, 22);
		contentPane.add(tfHolder);
		
		tfSum = new JTextField();
		tfSum.setColumns(10);
		tfSum.setBounds(134, 252, 125, 22);
		contentPane.add(tfSum);
		
		JRadioButton btnSaving = new JRadioButton("SAVING");
		btnSaving.setBounds(130, 30, 100, 25);
		contentPane.add(btnSaving);
		
		btnSpending = new JRadioButton("SPENDING");
		btnSpending.setBounds(234, 30, 94, 25);
		contentPane.add(btnSpending);
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg1) {
				String readIban,readHolder,readSum;
				int sum;
				readIban=tfIban.getText();
				readHolder=tfHolder.getText();
				readSum=tfSum.getText();
				//sum=Integer.parseInt(readSum);
				
				
				
				if (btnSaving.isSelected())
				{
					System.out.println("saving");
					
					Account sa = new SavingAccount (readIban,readHolder,0,0);
					
					bank.addAccount(sa);
				}
				
				if (btnSpending.isSelected())
				{
					System.out.println("spending");
					
					Account sp = new SpendingAccount (readIban,readHolder,0);
					bank.addAccount(sp);
					//System.out.println(sp.getIban());
					//System.out.println(sp.getHolder());
				}
				
				
				
				
			}
		});
		btnAdd.setFont(new Font("Arial", Font.BOLD, 15));
		btnAdd.setBounds(28, 182, 72, 38);
		contentPane.add(btnAdd);
		
		JButton btnUpd = new JButton("UPD");
		btnUpd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg2) {
				
				String readIban,readHolder,readSum;
				int sum;
				readIban=tfIban.getText();
				readHolder=tfHolder.getText();
				readSum=tfSum.getText();
				//sum=Integer.parseInt(readSum);
				
				
				
				if (btnSaving.isSelected())
				{
					
					Account sa = new SavingAccount (readIban,readHolder,0,0);
					
					bank.updateAccount(sa);
				}
				
				if (btnSpending.isSelected())
				{
					System.out.println("spending");
					
					Account sp = new SpendingAccount (readIban,readHolder,0);
					
					bank.updateAccount(sp);
					
					//System.out.println(sp.getIban());
					//System.out.println(sp.getHolder());
				}
				
			}
		});
		btnUpd.setFont(new Font("Arial", Font.BOLD, 15));
		btnUpd.setBounds(138, 182, 72, 38);
		contentPane.add(btnUpd);
		
		JButton btnRem = new JButton("REM");
		btnRem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg3) {
					
					String readHolder,readIban;
					readHolder=tfHolder.getText();
					readIban=tfIban.getText();
					bank.removeAccount(readHolder,readIban);
			}
		});
		btnRem.setFont(new Font("Arial", Font.BOLD, 15));
		btnRem.setBounds(256, 182, 72, 38);
		contentPane.add(btnRem);
		
		
		JLabel lblSum = new JLabel("SUM");
		lblSum.setFont(new Font("Arial", Font.PLAIN, 20));
		lblSum.setBounds(28, 238, 94, 46);
		contentPane.add(lblSum);
		
		JButton btnDeposit = new JButton("DEPOSIT");
		btnDeposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg4) {
				String readIban,readHolder,readSum;
				int sum;
				readIban=tfIban.getText();
				readHolder=tfHolder.getText();
				readSum=tfSum.getText();
				sum=Integer.parseInt(readSum);
				
				if (btnSaving.isSelected())
				{
					Account sa = new SavingAccount (readIban,readHolder,sum,0);
					
					bank.deposit(readIban,readHolder,sum);
				}
				if (btnSaving.isSelected())
				{
					
					Account sp = new SpendingAccount (readIban,readHolder,sum);
					
					bank.deposit(readIban,readHolder,sum);
					
				}
				
			}
		});
		btnDeposit.setBounds(68, 297, 111, 25);
		contentPane.add(btnDeposit);
		
		JButton btnWithdraw = new JButton("WITHDRAW");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg5) {
					String readsum,readholder,readiban;
					int sum;
					readholder=tfHolder.getText();
					readsum=tfSum.getText();
					readiban=tfIban.getText();
					sum=Integer.parseInt(readsum);
					bank.withdraw(sum,readholder,readiban);
					
			}
		});
		btnWithdraw.setBounds(209, 297, 106, 25);
		contentPane.add(btnWithdraw);
		
		JScrollPane tabelScrollPane = new JScrollPane();
		tabelScrollPane.setBounds(385, 54, 265, 166);
		contentPane.add(tabelScrollPane);
		
		JButton btnDisplay = new JButton("DISPLAY");
		btnDisplay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg8) {
				//bank.display();
				
				String [] col = {"IBAN","HOLDER","SUM"};
				DefaultTableModel dtm=new DefaultTableModel(col,0);
				
				table_1 = new JTable(dtm);
				tabelScrollPane.setViewportView(table_1);
				
				
				DefaultTableModel model = (DefaultTableModel) table_1.getModel();
				model.setRowCount(0);
				
				for (Client client:bank.HMap.keySet())
				{
						for(Account acc:bank.HMap.get(client))
						{
							model.addRow(new Object [] {acc.getIban(),acc.getHolder(),acc.getSum()});
						}
				}
				
				
				
				}
		});
		btnDisplay.setFont(new Font("Arial", Font.BOLD, 20));
		btnDisplay.setBounds(439, 246, 140, 38);
		contentPane.add(btnDisplay);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(523, 115, -62, -26);
		contentPane.add(scrollPane);
		
	
		
		table_1 = new JTable();
		tabelScrollPane.setViewportView(table_1);
		
		
		
		
	}
}
