
public class Account {

	
	private String iban;
	private String holder;
	private int sum;
	
	public Account ()
	{
		
	}
	
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
	}
	
	public Account (String iban,String holder,int sum)
	{
		this.iban=iban;
		this.holder=holder;
		this.sum=sum;
	}
	
	
	
	
	
	
}
